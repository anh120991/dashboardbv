﻿using System.Collections.Generic;

namespace WebCore.ViewModels.Role
{
    public class RoleRequestModel: BaseModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<RolePermissionRequestModel> PermissionIds { get; set; }
    }
}