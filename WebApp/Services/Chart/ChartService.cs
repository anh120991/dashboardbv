﻿using Base.Common;
using Base.Constants;
using Base.Model.Chart;
using Services.Common;
using Services.Token;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Chart
{
    public class ChartService : ClientServiceRequestBase, IChartService
    {
        private const string ApiGroupName = "Chart_Api";
        public ChartService(ITokenBaseService tokenBaseService = null) :
            base(ApiGroupName, tokenBaseService)
        {
        }

        public async Task<GenericResult<object>> GetChartOverview(OverviewRequestModel model)
        {
            return await PostAsync<GenericResult<object>, OverviewRequestModel>(model, Functions[ChartApiConstant.GetChartOverView], TokenString);
        }
    }
}
