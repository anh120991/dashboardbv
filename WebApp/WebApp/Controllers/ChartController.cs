﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Base.Common;
using Base.Model.Chart;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Chart;

namespace WebApp.Controllers
{
    public class ChartController : Controller
    {
        private readonly IChartService _chartService;

        public ChartController(IChartService chartService)
        {
            _chartService = chartService;
        }

        public async Task<JsonResult> GetChartOverview(string searchDate, int mpk)
        {
            var result = await _chartService.GetChartOverview(new OverviewRequestModel
            {
                FromDate = searchDate,
                Makp = mpk
            });
            return Json(result); 
        }
    }
}
