﻿using System;
using System.Data;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;

namespace WebCore.Models.StoreExtension
{
    public class CallStoreOracleExtension
    {
        public static async Task<DataTable> GetDataTable(string strSQL, params OracleParameter[] parameters)
        {
            var table = new DataTable();
            var cnnHelper = new ConnectionHelper();
            using (var cnn = cnnHelper.Connect())
            {
                try
                {
                    var cmd = new OracleCommand();
                    cmd.CommandText = strSQL;
                    cmd.CommandType = CommandType.StoredProcedure; ;
                    var adt = new OracleDataAdapter(cmd);
                    cmd.Connection = cnn;
                    //cmd.CommandTimeout = 0;

                    if (parameters != null)
                    {
                        AttachParameters(cmd, parameters);
                    }
                    table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                    await Task.Run(() => adt.Fill(table));
                }
                catch (OracleException sex)
                {
                    throw sex;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cnn.Close();
                }
            }
            return table;
        }

        private static void AttachParameters(OracleCommand oracleCommand, OracleParameter[] parameters)
        {
            foreach (var oracleParameter in parameters)
            {
                oracleCommand.Parameters.Add(oracleParameter);
            }
        }
    }
}
