﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Base.Constants
{
    public class RoleConstant
    {
        public const string AddNewRole = "AddNewRole";
        public const string GetAllRole = "GetAllRole";
        public const string UpdateRole = "UpdateRole";
        public const string GetDetailRole = "GetDetailRole";
        
    }
}
