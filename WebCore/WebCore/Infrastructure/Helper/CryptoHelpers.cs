﻿using System;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace WebCore.Infrastructure.Helper
{
    public static class CryptoHelpers
    {
        private const int SaltSize = 16;
        private const int DefaultHashIterations = 5000;

        /// <summary>
        /// Instance wide source for all random numbers.
        ///
        /// Only access through Random(), for locking purposes.
        /// </summary>
        private static readonly RandomNumberGenerator RandomSource = RandomNumberGenerator.Create();

        private static int? hashIterationsCached;

        /// <summary>
        /// Gets the iterations number to use when calculating a Secure or System hash.
        ///
        /// This value can vary over the lifetime of the site, so it should be stored.
        /// </summary>
        private static int HashIterations
        {
            get
            {
                if (!hashIterationsCached.HasValue)
                {
                    hashIterationsCached = DefaultHashIterations;
                }

                return hashIterationsCached.Value;
            }
        }

        /// <summary>
        /// Gets the hash of the input string.
        /// </summary>
        /// <param name="inputString">The input string.</param>
        /// <seealso cref="SHA256CryptoServiceProvider"/>
        public static string GetHash(string inputString)
        {
            HashAlgorithm hashAlgorithm = new SHA256CryptoServiceProvider();

            byte[] byteValue = Encoding.UTF8.GetBytes(inputString);

            byte[] byteHash = hashAlgorithm.ComputeHash(byteValue);

            return Convert.ToBase64String(byteHash);
        }

        /// <summary>
        /// Universal random provider.
        ///
        /// Just for paranoia's sake, use it for all random purposes.
        /// </summary>
        /// <param name="bytes">The number of bytes to random.</param>
        public static byte[] Random(int bytes)
        {
            var ret = new byte[bytes];

            lock (RandomSource)
            {
                RandomSource.GetBytes(ret);
            }

            return ret;
        }

        /// <summary>
        /// Backs Secure and System hashes.
        ///
        /// Uses PBKDF2 internally, as implemented by the Rfc2998DeriveBytes class.
        ///
        /// See http://en.wikipedia.org/wiki/PBKDF2
        /// and http://msdn.microsoft.com/en-us/library/bwx8t0yt.aspx.
        /// </summary>
        private static string Hash(string value, string salt)
        {
            var i = salt.IndexOf('.');
            var iters = int.Parse(salt.Substring(0, i), System.Globalization.NumberStyles.HexNumber);
            salt = salt.Substring(i + 1);

            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: value,
                salt: Convert.FromBase64String(salt),
                prf: KeyDerivationPrf.HMACSHA512,
                iterationCount: iters,
                numBytesRequested: 256 / 8));
            return hashed;
        }

        /// <summary>
        /// Cranks out a secure hash, generating a new salt.
        /// </summary>
        /// <param name="value">The value to encrypt.</param>
        /// <param name="salt">The salt for encrypt logic.</param>
        public static string SecureHash(string value, out string salt)
        {
            salt = GenerateSalt();

            return Hash(value, salt);
        }

        /// <summary>
        /// Cranks out a secure hash with a specific salt.
        /// </summary>
        /// <param name="value">The value to encrypt.</param>
        /// <param name="salt">The salt for encrypt logic.</param>
        public static string SecureHash(string value, string salt)
        {
            return Hash(value, salt);
        }

        /// <summary>
        /// Convenience wrapper around Random to grab a new salt value.
        /// Treat this value as opaque, as it captures iterations.
        /// </summary>
        /// <param name="explicitIterations">The numner of iteration for salt.</param>
        public static string GenerateSalt(int? explicitIterations = null)
        {
            if (explicitIterations.HasValue && explicitIterations.Value < DefaultHashIterations)
            {
                throw new ArgumentException("Cannot be less than " + DefaultHashIterations, "explicitIterations");
            }

            var bytes = Random(SaltSize);

            var iterations = (explicitIterations ?? HashIterations).ToString("X");

            return iterations + "." + Convert.ToBase64String(bytes);
        }
    }
}