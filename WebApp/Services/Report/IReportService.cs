﻿using Base.Common;
using Base.Model.Report;
using System.Threading.Tasks;

namespace Services.Report
{
    public interface IReportService
    {
        Task<GenericResult<object>> GetTotalHcStatus(HcStatusRequestModel model);
        Task<GenericResult<object>> GetHealthcareStatus();
    }
}
