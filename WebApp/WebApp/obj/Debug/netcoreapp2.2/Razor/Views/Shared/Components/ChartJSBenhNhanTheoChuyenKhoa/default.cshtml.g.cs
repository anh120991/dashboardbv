#pragma checksum "C:\Source\benhvien\webapp\WebApp\Views\Shared\Components\ChartJSBenhNhanTheoChuyenKhoa\default.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "7af5caf0128849cdf832ec573db93c1092a49dca"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared_Components_ChartJSBenhNhanTheoChuyenKhoa_default), @"mvc.1.0.view", @"/Views/Shared/Components/ChartJSBenhNhanTheoChuyenKhoa/default.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Shared/Components/ChartJSBenhNhanTheoChuyenKhoa/default.cshtml", typeof(AspNetCore.Views_Shared_Components_ChartJSBenhNhanTheoChuyenKhoa_default))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Source\benhvien\webapp\WebApp\Views\_ViewImports.cshtml"
using WebApp;

#line default
#line hidden
#line 2 "C:\Source\benhvien\webapp\WebApp\Views\_ViewImports.cshtml"
using WebApp.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7af5caf0128849cdf832ec573db93c1092a49dca", @"/Views/Shared/Components/ChartJSBenhNhanTheoChuyenKhoa/default.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1b56dbb1004b051eace1254c6910ad1099a51464", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared_Components_ChartJSBenhNhanTheoChuyenKhoa_default : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 2887, true);
            WriteLiteral(@"
<div class=""chart-container"" width=""600"" height=""400"">
    <canvas id=""chart2""></canvas>
</div>

<script>
    var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		var config = {
			type: 'line',
			title: 'Chuyên khoa',
			data: {
				labels: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
				datasets: [{
					label: 'Khoa nội',
                    backgroundColor: '#a93232',
                    borderColor: '#a93232',
					data: [
                         parseInt(Math.random() * 1000),
                         parseInt(Math.random() * 1000),
                         parseInt(Math.random() * 1000),
                         parseInt(Math.random() * 1000),
                         parseInt(Math.random() * 1000),
						 parseInt(Math.random() * 1000),
                         parseInt(Math.random() * 1000),
						");
            WriteLiteral(@" parseInt(Math.random() * 1000),
                         parseInt(Math.random() * 1000),
						 parseInt(Math.random() * 1000),
                         parseInt(Math.random() * 1000),
                             parseInt(Math.random() * 1000)
					],
					fill: false,
				}, {
					label: 'Khoa ngoại',
					fill: false,
                        backgroundColor: '#127d47',
                        borderColor: '#127d47',
					data: [
                        parseInt(Math.random() * 1000),
                        parseInt(Math.random() * 1000),
                        parseInt(Math.random() * 1000),
                        parseInt(Math.random() * 1000),
                        parseInt(Math.random() * 1000),
                        parseInt(Math.random() * 1000),
                        parseInt(Math.random() * 1000),
                        parseInt(Math.random() * 1000),
                        parseInt(Math.random() * 1000),
                        parseInt(Math.random() * 1000),
   ");
            WriteLiteral(@"                     parseInt(Math.random() * 1000),
                        parseInt(Math.random() * 1000)
					],
					}]
			},
			options: {
				responsive: true,
				title: {
					display: true
				},
				tooltips: {
					mode: 'index',
					intersect: false,
				},
				hover: {
					mode: 'nearest',
					intersect: true
				},
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Month'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Số người'
						}
					}]
				}
			}
		};
    document.addEventListener('DOMContentLoaded', (event) => {

        var ctx = document.getElementById('chart2');
        var myChart = new Chart(ctx, config );

	});

</script>

");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
