﻿using System.Data;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.Infrastructure.Extension;
using WebCore.Models;
using WebCore.Models.StoreExtension;
using WebCore.ViewModels;
using WebCore.ViewModels.Chart;

namespace WebCore.Services
{
    public class DepartmentService : IDepartmentService
    {
        public DepartmentService(DwhContext dwhContext)
        {
        }

        public async Task<GenericResult<DepartmentViewModel>> GetBTPK()
        {
            var parameters = new OracleParameter[1];
            parameters[0] = new OracleParameter("v_out", OracleDbType.RefCursor) {Direction = ParameterDirection.Output};
            var dt = await CallStoreOracleExtension.GetDataTable("get_btdkp", parameters);

            var result = dt.ConvertToList<DepartmentViewModel>();
            return GenericResult<DepartmentViewModel>.Succeed(result);
        }
    }
}