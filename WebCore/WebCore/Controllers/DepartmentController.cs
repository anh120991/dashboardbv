﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.Services;
using WebCore.ViewModels;

namespace WebCore.Controllers
{
    [Route("api/department")]
    [ApiController]
    public class DepartmentController : Controller
    {
        private readonly IDepartmentService _department;

        public DepartmentController(IDepartmentService department)
        {
            _department = department;
        }

        [HttpGet("v1.0.0/get-getbtpk")]
        public async Task<GenericResult<DepartmentViewModel>> GetBTPK()
        {
            var response = await _department.GetBTPK();
            return response;
        }
    }
}
