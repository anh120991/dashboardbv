﻿using System.Collections.Generic;

namespace WebCore.ViewModels.User
{
    public class UserResponseModel
    {
        public string Username { get; set; }
        public string Fullname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string DepartmentName { get; set; }
        public string Address { get; set; }
        public bool IsActive { get; set; }
        public List<long> RoleIds { get; set; }
        public long id { get; set; }
        public int Gender { get; set; }
        public long DepartmentId { get; set; }
    }
}