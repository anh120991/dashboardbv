﻿using System;

namespace WebCore.ViewModels.Report
{
    public class HcStatusRequestModel
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}