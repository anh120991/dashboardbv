﻿namespace WebCore.ViewModels
{
    public class LoggedUserDetailResponseModel
    {
        public string Id { get; set; }
        public string Avatar { get; set; }
        public string FullName { get; set; }
    }
}