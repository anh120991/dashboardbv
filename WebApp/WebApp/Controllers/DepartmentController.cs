﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Services.Department;


namespace WebApp.Controllers
{
    public class DepartmentController : Controller  
    {   
        private IDepartmentService _departmentService;

        public DepartmentController(IDepartmentService departmentService)
        {
            _departmentService = departmentService;   
        }
            
        public IActionResult Index()
        {
            return View();
        }
        public async Task<JsonResult> get_btdkp(string searchDate, int mpk)
        {
            var result = await _departmentService.GetBTPK();
           
            return Json(result);
        }
        
    }
}
