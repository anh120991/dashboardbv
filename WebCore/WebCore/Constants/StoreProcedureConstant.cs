﻿namespace WebCore.Constants
{
    public class StoreProcedureConstant
    {
        public const string SP_TOTAL_HC_STATUS = "PKG_REPORT.SP_TOTAL_HC_STATUS";
        public const string SP_HEALTHCARE_STATUS = "PKG_REPORT.SP_HEALTHCARE_STATUS";
    }
}