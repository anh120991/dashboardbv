﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Base.Model.Chart
{
    public class OverviewRequestModel
    {
        public string FromDate { get; set; }
        public decimal Makp { get; set; }
    }
}
