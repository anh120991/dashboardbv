﻿using Base.Common;
using Base.Model.User;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Department
{
    public interface IUserService
    {
        Task<GenericResult<object>> CreateNewUser(InsertUserRequestModel model);
        Task<GenericResult<object>> GetAllUser();
        Task<GenericResult<object>> GetDetailUser(long id);
    }
}
