﻿using System;
using System.Data;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;

namespace WebCore.Models.StoreExtension
{
    public class ConnectionHelper
    {
        public OracleConnection Connect()
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .Build();
            OracleConnection sqlConnection;
            var mainConnectionString = configuration.GetConnectionString("DwhContext");
            try
            {
                sqlConnection = new OracleConnection(mainConnectionString);
                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.Open();
                }
            }
            catch (OracleException sex)
            {
                //Log here
                throw sex;
            }
            return sqlConnection;
        }
    }
}