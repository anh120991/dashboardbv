﻿using Base.Model.Permission;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Services.Permission;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class PermissionController : Controller
    {
        IPermissionService _permissionService;
        string _permissionList = "tmpPermissionList";
        public PermissionController(IPermissionService permissionService)
        {
            _permissionService = permissionService;
        }
        public async Task<IActionResult> Index()
        {
            var result = await _permissionService.GetAllPermission();
            List<permissionViewModel> lsResult = new List<permissionViewModel>();
            if (result != null && result.Results.Count > 0)
            {
                foreach (var item in result.Results)
                {
                    permissionViewModel _dep = new permissionViewModel();
                    _dep = JsonConvert.DeserializeObject<permissionViewModel>(item.ToString());
                    lsResult.Add(_dep);
                }

            }
           
            return View(lsResult);
        }

        public IActionResult Create()
        {
            return View();
        }

        public IActionResult Update(string code)
        {
            if(TempData[_permissionList] != null)
            {

            }
            return View("Create");
        }

        public async Task<JsonResult> InsertPermission(PermissionRequestModel data)
        {

            var result = await _permissionService.CreatePermission(data);
            return Json(result);
        }
    }
}
