﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebCore.Constants;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.Infrastructure.Helper;
using WebCore.Models;
using WebCore.Models.Table;
using WebCore.Services.Interfaces;
using WebCore.ViewModels.User;

namespace WebCore.Services.Implementations
{
    public class UserService : IUserService
    {
        private readonly DwhContext _dwhContext;

        public UserService(DwhContext dwhContext)
        {
            _dwhContext = dwhContext;
        }



        public async Task<GenericResult<bool>> CreateUserAsync(UserRequestModel model)
        {
            if (model == null)
            {
                return GenericResult<bool>.Fail(SystemMessageConstant.RequestInvalid);
            }

            var passwordHash = CryptoHelpers.SecureHash(model.Password, out var salt);
            var user = new User
            {
                USERNAME = model.Username,
                ISACTIVE = 1,
                PASSWORDHASH = passwordHash,
                SALT = salt,
                ISLOCKOUT = 0,
                DELETED = 0,
                CREATEDDATE = DateTime.Now
            };

            var profile = new Profile
            {
                FULLNAME = model.Fullname,
                EMAIL = model.Email,
                ADDRESS = model.Address,
                AVATAR = model.Avatar,
                GENDER = model.Gender,
                DEPARTMENTID = model.DepartmentId,
                USER = user
            };
            user.Profiles = new List<Profile> { profile };

            if (model.RoleIds != null && model.RoleIds.Any())
            {
                user.UserRoles = model.RoleIds.Select(roleId => new UserRole
                {
                    RoleId = roleId

                }).ToList();
            }

            _dwhContext.Users.Add(user);
            await _dwhContext.SaveChangesAsync();

            return GenericResult<bool>.Succeeded;
        }

        public async Task<GenericResult<UserResponseModel>> GetAllUser()
        {
            var user = await (from u in _dwhContext.Users
                              join pr in _dwhContext.Profiles on u.ID equals pr.USERID
                              select new UserResponseModel
                              {
                                  Email = pr.EMAIL,
                                  Address = pr.ADDRESS,
                                  Fullname = pr.FULLNAME,
                                  IsActive = u.ISACTIVE == 1,
                                  Username = u.USERNAME,
                                  id = u.ID
                              }).ToListAsync();

            return GenericResult<UserResponseModel>.Succeed(user);
        }

        public async Task<GenericResult<UserResponseModel>> GetDetailUser(long id)
        {
            var user = await (from u in _dwhContext.Users.Include(s => s.UserRoles).Where(s => s.ID == id)
                              join pr in _dwhContext.Profiles on u.ID equals pr.USERID
                              select new UserResponseModel
                              {
                                  Email = pr.EMAIL,
                                  Address = pr.ADDRESS,
                                  Fullname = pr.FULLNAME,
                                  IsActive = u.ISACTIVE == 1,
                                  Username = u.USERNAME,
                                  Gender = pr.GENDER,
                                  DepartmentId = pr.DEPARTMENTID,
                                  RoleIds = u.UserRoles.Count > 0 ? u.UserRoles.Select(s => s.RoleId).ToList() : new List<long>()
                              }).FirstOrDefaultAsync();

            return GenericResult<UserResponseModel>.Succeed(user);
        }

        public async Task<GenericResult<bool>> UpdateUserAsync(UserRequestModel model)
        {
            if (model == null || model.Id == 0) return GenericResult<bool>.Failed;

            var user = await _dwhContext.Users.Include(s => s.UserRoles).FirstOrDefaultAsync(s => s.ID == model.Id);
            if (user == null) return GenericResult<bool>.Failed;

            user.ISACTIVE = model.IsActive ? 1 : 0;
            user.ISLOCKOUT = model.IsLockOut ? 1 : 0;
            if (model.IsLockOut)
                user.LOCKOUTEND = model.LockOutEnd;
            user.MODIFIEDDATE = DateTime.Now;
            var profile = await _dwhContext.Profiles.FirstOrDefaultAsync(s => s.USERID == model.Id);
            if (profile != null)
            {
                profile.ADDRESS = model.Address;
                profile.AVATAR = model.Avatar;
                profile.EMAIL = model.Email;
                profile.DEPARTMENTID = model.DepartmentId;
                profile.FULLNAME = model.Fullname;
                profile.GENDER = model.Gender;
            }

            var userRole = user.UserRoles;
            if (userRole != null && userRole.Count > 0)
            {
                userRole.Clear();
                userRole = model.RoleIds.Select(role => new UserRole
                {
                    UserId = model.Id,
                    RoleId = role
                }).ToList();

                user.UserRoles = userRole;
            }

            await _dwhContext.SaveChangesAsync();
            return GenericResult<bool>.Succeeded;
        }

        public async Task<GenericResult<bool>> ChangePasswordAsync(UserChangePasswordRequestModel model)
        {
            if (model == null) return GenericResult<bool>.Failed;

            var user = await _dwhContext.Users.Include(s => s.UserRoles).FirstOrDefaultAsync(s => s.ID == model.Id);
            if (user == null) return GenericResult<bool>.Failed;

            var pwdHash = CryptoHelpers.SecureHash(model.OldPassword, user.SALT);

            if (pwdHash != user.PASSWORDHASH)
            {
                return GenericResult<bool>.Fail("Username or password incorrect");
            }

            var passwordHash = CryptoHelpers.SecureHash(model.NewPassword, out var salt);
            user.PASSWORDHASH = passwordHash;
            user.SALT = salt;

            user.MODIFIEDDATE =DateTime.Now;
            await _dwhContext.SaveChangesAsync();
            return GenericResult<bool>.Succeeded;
        } 
    }
}