﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace WebCore.Infrastructure.Extension
{
    public static class AuthenticationExtension
    {
        public static long? GetUserId(this ClaimsPrincipal claimsIdentity)
        {
            if (claimsIdentity.Claims != null)
            {
                var userIdInToken = claimsIdentity.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;
                if (long.TryParse(userIdInToken, out long userId))
                {
                    return userId;
                }
            }

            return null;
        }
    }
}
