﻿namespace WebCore.ViewModels.Report
{
    public class HealthcareStatusResponseModel
    {
        public int HCS_ID { get; set; }
        public string HCS_CODE { get; set; }
        public string HCS_NAME { get; set; }
    }
}