﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Services.Department;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models;
using WebApp.Models.Chart;


namespace WebApp.ViewComponents
{
    [ViewComponent(Name = "departmentList")]
    public class departmentList : ViewComponent
	{
        private IDepartmentService _departmentService;

        public departmentList(IDepartmentService departmentService)
        {
            _departmentService = departmentService;
        }



        public async Task<IViewComponentResult> InvokeAsync(int departmentSelect)
        {
            List<departmentViewModel> lsResult = new List<departmentViewModel>();
            var result = await _departmentService.GetBTPK();
            if (result != null && result.Results.Count > 0)
            {
                foreach (var item in result.Results)
                {
                    departmentViewModel _dep = new departmentViewModel();
                    _dep = JsonConvert.DeserializeObject<departmentViewModel>(item.ToString());
                    lsResult.Add(_dep);
                }

            }
            return View(lsResult);
        }
    }
}
