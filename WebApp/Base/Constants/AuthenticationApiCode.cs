﻿namespace Base.Constants
{
    public class AuthenticationApiCode
    {
        public const string GetRefreshToken = "GetRefreshToken";
        public const string GetToken = "GetToken";
        public const string GetCurrentUserDetail = "GetCurrentUserDetail";
        public const string Logout = "Logout";
    }
}