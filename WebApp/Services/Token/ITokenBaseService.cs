﻿namespace Services.Token
{
    public interface ITokenBaseService
    {
        string GetToken();
    }
}