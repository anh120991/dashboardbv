﻿using System;

namespace WebCore.ViewModels
{
    public class BaseModel
    {
        public long Id { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long ModifiedBy { get; set;}
        public DateTime? ModifiedDateTime { get; set; }
    }
}
