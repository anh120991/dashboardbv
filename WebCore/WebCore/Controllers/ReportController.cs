﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.Services.Interfaces;
using WebCore.ViewModels.Report;

namespace WebCore.Controllers
{
    [Route("api/report")]
    [Authorize]
    public class ReportController : Controller
    {
        private readonly IReportService _report;

        public ReportController(IReportService report)
        {
            _report = report;
        }

        [HttpPost("v1.0.0/get-total-hc-status")]
        public async Task<GenericResult<HcStatusResponseModel>> GetTotalHcStatusAsync([FromBody]HcStatusRequestModel model)
        {
            return await _report.GetTotalHCStatusAsync(model);
        }

        [HttpGet("v1.0.0/get-total-hc-status")]
        public async Task<GenericResult<HealthcareStatusResponseModel>> GetHealthcareStatusAsync()
        {
            return await _report.GetHealthcareStatusAsync();
        }
    }
}