﻿using System;

namespace WebCore.ViewModels.Report
{
    public class HcStatusResponseModel
    {
        public DateTime OP_DATE { get; set; }
        public int OP_HCS_ID { get; set; }
        public int OP_PATIENTS { get; set; }
        public int OP_EXAMS { get; set; }
    }
}