﻿using System.Threading.Tasks;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.ViewModels;
using WebCore.ViewModels.Chart;

namespace WebCore.Services
{
    public interface IDepartmentService
    {
        Task<GenericResult<DepartmentViewModel>> GetBTPK();
    }
}