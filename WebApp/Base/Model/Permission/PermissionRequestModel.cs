﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Base.Model.Report
{
    public class HcStatusRequestModel
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
