﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Base.Model.User
{
    public class InsertUserRequestModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Fullname { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Avatar { get; set; }
        public int DepartmentId { get; set; }
        public int Gender { get; set; }
    }
}
