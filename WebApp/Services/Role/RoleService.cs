﻿using Base.Common;
using Base.Constants;
using Base.Model.Chart;
using Base.Model.Role;
using Base.Model.User;
using Services.Common;
using Services.Token;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Role
{
    public class RoleService : ClientServiceRequestBase, IRoleService
    {
        private const string ApiGroupName = "Role_Api";
        public RoleService(ITokenBaseService tokenBaseService = null) :
            base(ApiGroupName, tokenBaseService)
        {
        }

        public async Task<GenericResult<object>> CreateRole(RoleRequestModel model)
        {
            return await PostAsync<GenericResult<object>, RoleRequestModel>(model, Functions[RoleConstant.AddNewRole], token: TokenString);
        }

        public async Task<GenericResult<object>> GetAll()
        {
            return await GetAsync<GenericResult<object>>(Functions[RoleConstant.GetAllRole], token: TokenString);
        }

        public async Task<GenericResult<object>> GetDetailRole(long id)
        {
            return await GetAsync<GenericResult<object>>(string.Format(Functions[RoleConstant.GetDetailRole], id), token: TokenString);
        }

        public async Task<GenericResult<object>> UpdateRole(RoleRequestModel model)
        {
            return await PostAsync<GenericResult<object>, RoleRequestModel>(model, Functions[RoleConstant.UpdateRole], token: TokenString);
        }
    }
}
