﻿using System.Data;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.Infrastructure.Extension;
using WebCore.Models;
using WebCore.Models.StoreExtension;
using WebCore.ViewModels;
using WebCore.ViewModels.Chart;

namespace WebCore.Services
{
    public class ChartService : IChartService
    {
        public ChartService(DwhContext dwhContext)
        {
        }

        public async Task<GenericResult<OverviewViewModel>> GetChartOverview(OverviewRequestModel model)
        {
            var parameters = new OracleParameter[3];
            int i = 0;
            parameters[i++] = new OracleParameter("v_date", model.FromDate);
            parameters[i++] = new OracleParameter("v_makp", model.Makp);
            parameters[i++] = new OracleParameter("v_out", OracleDbType.RefCursor) {Direction = ParameterDirection.Output};
            var dt = await CallStoreOracleExtension.GetDataTable("get_overview", parameters);

            var result = dt.ConvertToList<OverviewViewModel>();
            return GenericResult<OverviewViewModel>.Succeed(result);
        }
    }
}