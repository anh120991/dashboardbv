﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class departmentViewModel
    {
        public long id { get; set; }
        public string ma { get; set; }
        public string ten { get; set; }
        public string isactive { get; set; }
    }
}
