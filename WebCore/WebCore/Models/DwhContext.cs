﻿using Microsoft.EntityFrameworkCore;
using WebCore.Models.Table;

namespace WebCore.Models
{
    public class DwhContext : DbContext
    {
        public DwhContext(DbContextOptions<DwhContext> options)
            : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Profile> Profiles { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<Role> Roles { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(entity =>
            {

                entity.Property(e => e.USERNAME)
                    .IsRequired()
                    .HasMaxLength(512);


                entity.HasMany(o => o.Profiles)
                    .WithOne(p => p.USER)
                    .HasForeignKey(d => d.USERID)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<UserRole>().HasKey(sc => new { sc.RoleId, sc.UserId });
            modelBuilder.Entity<RolePermissions>().HasKey(sc => new { sc.ROLEID, sc.PERMISSIONID });

            modelBuilder.Entity<UserRole>()
                .HasOne(bc => bc.User)
                .WithMany(b => b.UserRoles)
                .HasForeignKey(bc => bc.UserId);

            modelBuilder.Entity<UserRole>()
                .HasOne(bc => bc.Role)
                .WithMany(c => c.UserRoles)
                .HasForeignKey(bc => bc.RoleId);

            modelBuilder.Entity<RolePermissions>()
                .HasOne(bc => bc.Role)
                .WithMany(b => b.RolePermissions)
                .HasForeignKey(bc => bc.ROLEID);

            modelBuilder.Entity<RolePermissions>()
                .HasOne(bc => bc.Permission)
                .WithMany(c => c.RolePermissions)
                .HasForeignKey(bc => bc.PERMISSIONID);
        }
    }
}
