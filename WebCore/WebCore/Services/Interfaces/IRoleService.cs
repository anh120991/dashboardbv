﻿using System.Threading.Tasks;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.ViewModels.Role;

namespace WebCore.Services.Interfaces
{
    public interface IRoleService
    {
        Task<GenericResult<RoleResponseModel>> GetAllRoleAsync();
        Task<GenericResult<bool>> CreateNewRoleAsync(RoleRequestModel model);
        Task<GenericResult<bool>> UpdateRoleAsync(RoleRequestModel model);
        Task<GenericResult<RoleResponseModel>> GetDetailRoleAsync(long id);
    }
}