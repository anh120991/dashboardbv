﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace WebApp.Common
{
    public static class Uitil
    {
        private static Random random = new Random();
        public static void TransferData<TSource, TTarget>(TSource source, ref TTarget target)

        {

            TransferData(source, ref target, null, new string[] { });

        }



        /// <summary>

        /// TransferList Data object

        /// </summary>

        /// <typeparam name="TSource"></typeparam>

        /// <typeparam name="TTarget"></typeparam>

        /// <param name="source"></param>

        /// <param name="target"></param>

        public static void TransferData<TSource, TTarget>(List<TSource> source, ref List<TTarget> target)

        {

            target = new List<TTarget>();

            foreach (var _source in source)

            {

                var _target = (TTarget)Activator.CreateInstance(typeof(TTarget));

                TransferData(_source, ref _target, null, new string[] { });

                target.Add(_target);

            }

        }



        private static void TransferData<TSource, TTarget>(TSource source, ref TTarget target, Type _targetType, object include)

        {

            if (source == null)

                return;

            string[] includeProperty = include?.GetType().GetProperties().Select(a => a.Name).ToArray() ?? new string[] { };

            PropertyInfo[] __propSource = source.GetType().GetProperties().ToArray();



            Type targetObject = typeof(TTarget);

            if (_targetType != null)

            {

                targetObject = _targetType;

                target = (dynamic)Activator.CreateInstance(_targetType);

            }



            foreach (var propSource in __propSource)

            {

                var propTarget = targetObject.GetProperty(propSource.Name);

                if (propTarget != null)

                {

                    var sourceType = propSource.PropertyType;

                    var targetType = propTarget.PropertyType;



                    if (sourceType.GetInterfaces().Any(a => a.IsGenericType &&

                            a.GetGenericTypeDefinition() == typeof(IEnumerable<>))

                        && includeProperty.Contains(propSource.Name))

                    {

                        //Transfer child as list

                        Type _type2 = targetType.GetGenericArguments()[0];

                        var _data1 = propSource.GetValue(source);

                        var listType = typeof(List<>);

                        var constructedListType = listType.MakeGenericType(_type2);

                        var _data2 = Activator.CreateInstance(constructedListType);



                        //Get child object include

                        var includeChild = include.GetType().GetProperty(propSource.Name)?.GetValue(include);



                        foreach (var item1 in (dynamic)_data1)

                        {

                            dynamic item2 = (dynamic)Activator.CreateInstance(_type2);

                            TransferData(item1, ref item2, _type2, includeChild);

                            _data2.GetType().GetMethod("Add").Invoke(_data2, new[] { item2 });

                        }

                        propTarget.SetValue(target, _data2);

                    }

                    else if (sourceType.Namespace.StartsWith("OCB.Treasury")

                        && targetType.Namespace.StartsWith("OCB.Treasury")

                        && sourceType.IsClass

                        && targetType.IsClass

                        && includeProperty.Contains(propSource.Name))

                    {

                        //Transfer child as class object

                        var _data1 = propSource.GetValue(source);

                        dynamic item2 = (dynamic)Activator.CreateInstance(targetType);



                        //Get child object include

                        var includeChild = include.GetType().GetProperty(propSource.Name)?.GetValue(include);



                        TransferData(_data1, ref item2, targetType, includeChild);

                        propTarget.SetValue(target, item2);

                    }

                    else if (sourceType == targetType

                        || (sourceType.IsGenericType && sourceType.GetGenericTypeDefinition() == typeof(Nullable<>)

                            && Nullable.GetUnderlyingType(sourceType) == targetType)

                        || (targetType.IsGenericType && targetType.GetGenericTypeDefinition() == typeof(Nullable<>)

                            && Nullable.GetUnderlyingType(targetType) == sourceType))

                    {

                        var _data = propSource.GetValue(source);

                        propTarget.SetValue(target, _data);

                    }

                }

            }

        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

       
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
