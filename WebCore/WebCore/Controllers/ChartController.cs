﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.Services;
using WebCore.ViewModels;
using WebCore.ViewModels.Chart;

namespace WebCore.Controllers
{
    [Route("api/chart")]
    [ApiController]
    public class ChartController : Controller
    {
        private readonly IChartService _chart;

        public ChartController(IChartService chart)
        {
            _chart = chart;
        }

        [HttpPost("v1.0.0/get-overview")]
        public async Task<GenericResult<OverviewViewModel>> GetChartOverview(OverviewRequestModel model)
        {
            var response = await _chart.GetChartOverview(model);
            return response;
        }
    }
}