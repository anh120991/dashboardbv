﻿namespace Base.Constants
{
    public class ApplicationKey
    {
        public const string UserInfo = "UserInfo";
        public const string RememberMe = "RememberMe";
        public const string RememberEmail = "Email";
        public const string Token = "Token";
        public const string LoggedInFlag = "LoggedInFlag";
        public const string CurrentEmployeeId = "CurrentEmployeeId";
    }
}