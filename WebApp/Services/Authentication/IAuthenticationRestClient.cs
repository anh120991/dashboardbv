﻿using System.Threading.Tasks;
using Base.Common;
using Base.Model.Authentication;
using Base.Model.Identity;

namespace Services.Authentication
{
    public interface IAuthenticationRestClient
    {
        GenericResult<LoginResponseModel> GetToken(LoginRequestModel request);
        GenericResult<LoginResponseModel> RefreshToken(RefreshTokenRequestModel request);
        Task<GenericResult<LoggedUserDetailResponseModel>> GetCurrentUserDetailAsync(string token);
        GenericResult<bool> Logout(string token);
    }
}