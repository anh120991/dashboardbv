﻿using Base.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Department
{
    public interface IDepartmentService
    {
        Task<GenericResult<object>> GetBTPK();
    }
}
