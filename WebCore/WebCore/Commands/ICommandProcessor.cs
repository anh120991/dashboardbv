﻿using System.Threading.Tasks;

namespace WebCore.Commands
{
    public interface ICommandProcessor
    {
        Task ProcessAsync<T>(T command)
            where T : ICommand;
    }
}