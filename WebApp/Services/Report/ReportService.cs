﻿using Base.Common;
using Base.Constants;
using Base.Model.Chart;
using Base.Model.Report;
using Base.Model.Role;
using Base.Model.User;
using Services.Common;
using Services.Token;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Report
{
    public class ReportService : ClientServiceRequestBase, IReportService
    {
        private const string ApiGroupName = "Report_Api";
        public ReportService(ITokenBaseService tokenBaseService = null) :
            base(ApiGroupName, tokenBaseService)
        {
        }


        public async Task<GenericResult<object>> GetHealthcareStatus()
        {
            return await GetAsync<GenericResult<object>>( Functions[ReportConstant.GetHealthcareStatus], token: TokenString);
        }

        public async Task<GenericResult<object>> GetTotalHcStatus(HcStatusRequestModel model)
        {
            return await PostAsync<GenericResult<object>, HcStatusRequestModel>(model, Functions[ReportConstant.GetTotalHcStatus], token: TokenString);
        }

    }
}
