﻿using Base.Model.User;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Services.Department;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Common;
using WebApp.Models;
using WebApp.Models.DataModels;


namespace WebApp.Controllers
{
    public class UserController : Controller
    {
        private IUserService _userService;
        public UserController(IUserService userService) {
            _userService = userService;
        }
        public async Task<IActionResult> Index()
        {
            
            var result = await _userService.GetAllUser();
            List<userViewModel> lsResult = new List<userViewModel>();
            if (result != null && result.Results.Count > 0)
            {
                foreach (var item in result.Results)
                {
                    userViewModel _dep = new userViewModel();
                    _dep = JsonConvert.DeserializeObject<userViewModel>(item.ToString());
                    lsResult.Add(_dep);
                }

            }
            return View(lsResult);
        }

        public IActionResult Create()
        {
            userViewModel model = new userViewModel();
            return View(model);
        }

        public IActionResult ChangePassword()
        {
            return View();
        }

        public async Task<IActionResult> Update(long id)
        {
            var result = await _userService.GetDetailUser(id);
            userViewModel model = new userViewModel();
            if (result != null)
            {
                model = JsonConvert.DeserializeObject<userViewModel>(result.Results[0].ToString());
            }
            return View("Create", model);
        }

        public async Task<JsonResult> InsertUser(UserModel data)
        {
            InsertUserRequestModel requestModel = new InsertUserRequestModel();
            string rdPass = Uitil.RandomString(10);
            data.Password = rdPass;
            Uitil.TransferData(data, ref requestModel);
            var result = await _userService.CreateNewUser(requestModel);
            result.Metadata.Message = rdPass;
            return Json(result);
        }
    }
}
