﻿using Base.Common;
using Base.Model.Chart;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Chart
{
   public interface IChartService
    {
        Task<GenericResult<object>> GetChartOverview(OverviewRequestModel model);
    }
}
