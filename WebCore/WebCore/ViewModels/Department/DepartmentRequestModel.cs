﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebCore.ViewModels
{
    public class DepartmentViewModel
    {
        public long ID { get; set; }
        public string MA { get; set; }
        public string TEN { get; set; }
        public int ISACTIVE { get; set; }
    }
}
