﻿using System.Threading.Tasks;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.ViewModels.Report;

namespace WebCore.Services.Interfaces
{
    public interface IReportService
    {
        Task<GenericResult<HcStatusResponseModel>> GetTotalHCStatusAsync(HcStatusRequestModel model);
        Task<GenericResult<HealthcareStatusResponseModel>> GetHealthcareStatusAsync();
    }
}