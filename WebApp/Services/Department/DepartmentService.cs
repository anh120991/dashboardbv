﻿using Base.Common;
using Base.Constants;
using Base.Model.Chart;
using Services.Common;
using Services.Token;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Department
{
    public class DepartmentService : ClientServiceRequestBase, IDepartmentService
    {
        private const string ApiGroupName = "Department_Api";
        public DepartmentService(ITokenBaseService tokenBaseService = null) :
            base(ApiGroupName, tokenBaseService)
        {
        }

        public async Task<GenericResult<object>> GetBTPK()
        {
            return await GetAsync<GenericResult<object>>(Functions[DepartmentConstant.GetBTDKP], token: TokenString);
            
        }


    }
}
