﻿using System.Collections.Generic;

namespace WebCore.ViewModels.Role
{
    public class RolePermissionRequestModel
    {
        public long RoleId { get; set; }
        public long PermissionId { get; set; }
        public bool IsEnabled { get; set; }
    }
}