﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Base.Common;
using Base.Constants;
using Base.Model.Authentication;
using Base.Model.Identity;
using Services.Common;
using Services.Token;

namespace Services.Authentication
{
    public class AuthenticationRestClient : ClientServiceRequest, IAuthenticationRestClient
    {
        private const string ApiGroupName = "Authentication_Api";
        public override string RestPath { get; set; }
        public override string BaseUri { get; set; }
        public override int Timeout { get; set; }
        public Dictionary<string, string> Functions { get; set; }

        public AuthenticationRestClient()
        {
            var api = ApiSetting.Apis.FirstOrDefault(s => s.Key == ApiGroupName);
            if (api != null && api.Functions.Any())
            {
                BaseUri = api.Address;
                Functions = api.Functions.ToDictionary(s => s.Code, s => s.Patch);
            }

            Init();
        }

        public GenericResult<LoginResponseModel> GetToken(LoginRequestModel request)
        {
            return Post<GenericResult<LoginResponseModel>, LoginRequestModel>(request, Functions[AuthenticationApiCode.GetToken]);
        }

        public GenericResult<LoginResponseModel> RefreshToken(RefreshTokenRequestModel request)
        {
            return Post<GenericResult<LoginResponseModel>, RefreshTokenRequestModel>(request, Functions[AuthenticationApiCode.GetRefreshToken]);
        }

        public async Task<GenericResult<LoggedUserDetailResponseModel>> GetCurrentUserDetailAsync(string token)
        {
            if (string.IsNullOrEmpty(token))
            {
            }

            return await GetAsync<GenericResult<LoggedUserDetailResponseModel>>(Functions[AuthenticationApiCode.GetCurrentUserDetail], token: token);
        }
        public GenericResult<bool> Logout(string token)
        {
            return Delete<GenericResult<bool>>(Functions[AuthenticationApiCode.Logout], token);
        }
    }
}