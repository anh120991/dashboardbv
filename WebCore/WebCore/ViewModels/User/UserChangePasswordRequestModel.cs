﻿namespace WebCore.ViewModels.User
{
    public class UserChangePasswordRequestModel
    {
        public long Id { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}