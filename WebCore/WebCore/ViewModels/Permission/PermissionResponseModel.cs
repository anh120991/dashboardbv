﻿namespace WebCore.ViewModels.Permission
{
    public class PermissionResponseModel: BaseModel
    {
        public string Code { get; set; }
        public string Parent { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public long DisplayOrder { get; set; }
        public bool IsEnabled { get; set; }
    }
}