﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebCore.ViewModels.Permission;

namespace WebCore.ViewModels.Role
{
    public class RoleResponseModel : BaseModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Guid { get; set; }

        public List<PermissionResponseModel> Permissions { get; set; }
    }
}
