﻿using System.Data;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.Services.Interfaces;
using WebCore.ViewModels;

namespace WebCore.Controllers
{
    [Route("api/authentication")]
    public class AuthenticationController : Controller
    {
        // GET
        private readonly IAuthenticationAppService _authenticationService;

        public AuthenticationController(IAuthenticationAppService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        [AllowAnonymous]
        [HttpPost("v1.0.0/login")]
        public async Task<GenericResult<LoginResponseModel>> Login([FromBody]LoginRequestModel model)
        {
            var response = await _authenticationService.AuthenticateWithUserNamePassword(model);
            return response;
        }

        [HttpPost("v1.0.0/refresh-login")]
        public async Task<GenericResult<LoginResponseModel>> RefreshToken([FromBody] RefreshTokenRequestModel model)
        {
            var response = await _authenticationService.AuthenticateWithRefreshToken(model);
            return response;
        }

        
        [HttpDelete("v1.0.0/logout")]
        public async Task<GenericResult<bool>> Logout()
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token");
            var response = await _authenticationService.Logout(accessToken);
            return response;
        }

        [Authorize]
        [HttpGet("v1.0.0/get-details")]
        public async Task<GenericResult<LoggedUserDetailResponseModel>> GetDetails()
        {
            GenericResult<LoggedUserDetailResponseModel> response = await _authenticationService.GetCurrentUserDetailAsync();
            return response;
        }
    }
}