﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace WebCore.Infrastructure.Caching
{
    public class CacheService : ICacheService
    {
        private static ConnectionMultiplexer _connection;
        private readonly IServer _server;

        public CacheService(string cacheConnection)
        {
            var lazyConnection = new Lazy<ConnectionMultiplexer>(() => ConnectionMultiplexer.Connect(cacheConnection));
            _connection = lazyConnection.Value;
            _server = _connection.GetServer(_connection.GetEndPoints().FirstOrDefault());
        }

        public async Task<T> GetByCacheKeyAsync<T>(string cacheKey)
        {
            return Deserialize<T>(await _connection.GetDatabase().StringGetAsync(cacheKey));
        }

        public async Task<IEnumerable<T>> GetByRedisKeysAsync<T>(RedisKey[] keys)
        {
            var result = await _connection.GetDatabase().StringGetAsync(keys);
            return Deserialize<T>(result.Select(x => (string)x));
        }

        public async Task SetByCacheKeyAsync(string cacheKey, object cacheValue)
        {
            await _connection.GetDatabase().StringSetAsync(cacheKey, Serialize(cacheValue));
        }

        public async Task SetByCacheKeyWithExpireTimeAsync(string cacheKey, object cacheValue, TimeSpan expireTime)
        {
            await _connection.GetDatabase().StringSetAsync(cacheKey, Serialize(cacheValue), expireTime);
        }

        public async Task<RedisKey[]> GetKeysAsync(string pattern)
        {
            var keys = _server.Keys(pattern: pattern);
            return await Task.FromResult(keys.ToArray());
        }

        public async Task DeleteAsync(string cachekey)
        {
            await _connection.GetDatabase().KeyDeleteAsync(cachekey);
        }

        private static string Serialize(object model)
        {
            return model == null ? null : JsonConvert.SerializeObject(model);
        }

        private static T Deserialize<T>(string jsonObject)
        {
            return jsonObject == null ? default : JsonConvert.DeserializeObject<T>(jsonObject);
        }

        private static List<T> Deserialize<T>(IEnumerable<string> objectList)
        {
            var list = new List<T>();
            if (objectList == null) return list;

            list.AddRange(objectList.Select(JsonConvert.DeserializeObject<T>));

            return list;
        }
    }
}