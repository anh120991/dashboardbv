﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using StackExchange.Redis;

namespace WebCore.Infrastructure.Caching
{
    public interface ICacheService
    {
        Task<T> GetByCacheKeyAsync<T>(string cacheKey);
        Task SetByCacheKeyAsync(string cacheKey, object cacheValue);
        Task SetByCacheKeyWithExpireTimeAsync(string cacheKey, object cacheValue, TimeSpan expireTime);
        Task<RedisKey[]> GetKeysAsync(string pattern);
        Task<IEnumerable<T>> GetByRedisKeysAsync<T>(RedisKey[] keys);
        Task DeleteAsync(string cachekey);
    }
}