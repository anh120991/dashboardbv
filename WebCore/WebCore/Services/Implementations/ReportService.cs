﻿using System.Data;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using WebCore.Constants;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.Infrastructure.Extension;
using WebCore.Models.StoreExtension;
using WebCore.Services.Interfaces;
using WebCore.ViewModels;
using WebCore.ViewModels.Report;

namespace WebCore.Services.Implementations
{
    public class ReportService : IReportService
    {
        public async Task<GenericResult<HcStatusResponseModel>> GetTotalHCStatusAsync(HcStatusRequestModel model)
        {
            var parameters = new OracleParameter[3];
            int i = 0;
            parameters[i++] = new OracleParameter("P_FRM_DATE", model.FromDate?.ToString("dd/MM/yyyy"));
            parameters[i++] = new OracleParameter("P_TO_DATE", model.ToDate?.ToString("dd/MM/yyyy"));
            parameters[i++] = new OracleParameter("P_RS", OracleDbType.RefCursor) { Direction = ParameterDirection.Output };

            var dt = await CallStoreOracleExtension.GetDataTable(StoreProcedureConstant.SP_TOTAL_HC_STATUS, parameters);

            var result = dt.ConvertToList<HcStatusResponseModel>();
            return GenericResult<HcStatusResponseModel>.Succeed(result);
        }

        public async Task<GenericResult<HealthcareStatusResponseModel>> GetHealthcareStatusAsync()
        {
            var parameters = new OracleParameter[1];
            int i = 0;
            parameters[i++] = new OracleParameter("P_RS", OracleDbType.RefCursor) { Direction = ParameterDirection.Output };

            var dt = await CallStoreOracleExtension.GetDataTable(StoreProcedureConstant.SP_HEALTHCARE_STATUS, parameters);

            var result = dt.ConvertToList<HealthcareStatusResponseModel>();
            return GenericResult<HealthcareStatusResponseModel>.Succeed(result);
        }
    }
}