﻿namespace WebCore.ViewModels.Chart
{
    public class OverviewRequestModel
    {
        public string FromDate { get; set; }
        public decimal Makp { get; set; }
    }
}